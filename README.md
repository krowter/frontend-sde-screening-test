# README #

Hi friends, this is a test for junior frontend developer. 

Please follow the steps and goal to be successfull candidate.

### What to make? ###

* Use HTML and Javascript or PHP
* Call this api http://api.tvmaze.com/search/shows?q=girls and use the json from it. The main properties are url and name, you can use other properties as creative as you want. But the main properties must displayed in UI  
* Display the data using this design https://imgur.com/MtilNpm
* Explanation of design in above url (https://imgur.com/MtilNpm) : 
* 1 Hardcode text Campflix bold style
* 2 Textbox for search and profile name / user name just fill your name e.g budi erwanto
* 3 Display a picture from the api using url property can be 1st image in array or any random up to you
* 4 Display all tv shows from this api http://api.tvmaze.com/search/shows?q=girls, scrollable to the right
* 5 Bonus if you can implement search api, but not mandatory 

### How to submit? ###

* Use this bitbucket repository as a place for your submission
* Create a new branch with this format : junior-fe-<insert-your-name>. e.g : junior-fe-budierwanto
* Push your code to that branch 

### Deadline ###

* 7 days from the test given

